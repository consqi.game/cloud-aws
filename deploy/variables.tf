variable "prefix" {
  default = "tnlm"
}

variable "project" {
  default = "cloud-aws-tnlm"
}

variable "contact" {
  default = "bci@bciasia.co.id"
}
